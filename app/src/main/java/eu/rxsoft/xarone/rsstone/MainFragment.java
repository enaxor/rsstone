package eu.rxsoft.xarone.rsstone;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.webkit.URLUtil;
import android.widget.Toast;

import org.cobaltians.cobalt.Cobalt;
import org.cobaltians.cobalt.fragments.CobaltFragment;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSItem;
import org.mcsoxford.rss.RSSReader;

import java.util.List;
import java.util.StringTokenizer;

/**
 * rsstone
 * Created by @xarone on 10/24/16.
 */

public class MainFragment extends CobaltFragment {

    private static final String TAG = "MainFragment";
    private ProgressDialog dialog;

    /**
     * Parse an RSSFeed object as a JSONObject
     * @param feed RSSFeed object
     * @param uri user's input uri
     * @return a JSONObject containing feed data
     */
    private JSONObject parseFeed(RSSFeed feed, String uri) {
        // get latest feeds from the RSS flux
        JSONArray feeds = new JSONArray();
        for (RSSItem item : feed.getItems()) {
            JSONObject itemObject = new JSONObject();
            try {
                itemObject.put("title", item.getTitle());
                itemObject.put("content", item.getContent());
                itemObject.put("description", item.getDescription());
                itemObject.put("content", item.getContent());
                itemObject.put("date", (item.getPubDate() == null ? "" : item.getPubDate().toString()));
                itemObject.put("thumbnail", (item.getThumbnails().size() > 0 ? item.getThumbnails().get(0).toString() : ""));
                itemObject.put("link", item.getLink());
            } catch (JSONException e) {
                Log.e(TAG, "Json error when getting feeds for " + uri + ".");
                e.printStackTrace();
            }
            feeds.put(itemObject);
        }
        // get RSS details
        JSONObject RSS = new JSONObject();
        try {
            RSS.put("title", feed.getTitle());
            RSS.put("link", feed.getLink().toString());
            RSS.put("count", feed.getItems().size());
            RSS.put("feedUrl", uri);
            RSS.put("feeds", feeds);
        } catch (JSONException e) {
            Log.e(TAG, "Json error when getting RSS flux for " + uri + ".");
            e.printStackTrace();
        }
        return RSS;
    }

    private void getRss() {
        if (!isInternetConnection()) {
            notifyRssUpdateError(getResources().getString(R.string.err_connection));
            return;
        }
        final RSSReader reader = new RSSReader();
        final List<String> rssUrls = DatabaseHelper.getInstance(getContext()).getRssUrl();
        Thread getRssThread = new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject feeds = new JSONObject();
                RSSFeed feed = null;
                int i = 0;
                for (final String uri : rssUrls) {
                    // check of url validity has be done at url adding
                    if (Cobalt.DEBUG) Log.d(TAG, "Processing " + uri);
                    try {
                        feed = reader.load(uri);  // Download rss feeds
                        if (feed == null) throw new NullPointerException("Feeds are empty.");
                    } catch (Exception e) {
                        notifyRssUpdateError(getResources().getString(R.string.err_processing) +
                                " " + uri + "\n > " + new StringTokenizer(e.getMessage(), ":").nextToken());
                        incrementProgressDialog();
                        continue;
                    }
                    JSONObject RSS = parseFeed(feed, uri); // parse feed
                    try {
                        feeds.put(Integer.toString(i), RSS);
                        if (Cobalt.DEBUG)
                            Log.d(TAG, "[" + i + "] " + RSS.get("title") + "RSS flux get " + RSS.getJSONArray("feeds").length() + " new feeds.");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    incrementProgressDialog();
                    i++;
                }
                // feeds is ready
                sendEvent("feeds", feeds, "callback");
                closeProgressDialog();
                notifyRssUpdateSuccess(getResources().getString(R.string.done_processing));
            }
        });
        openProgressDialog(rssUrls.size());
        getRssThread.start();
    }

    /**
     * Progress dialog methods
     */
    void openProgressDialog(final int progressBarSize) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                dialog = new ProgressDialog(getContext());
                dialog.setMax(progressBarSize);
                dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                dialog.setMessage(getResources().getString(R.string.wait_processing));
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
    }

    void incrementProgressDialog() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                dialog.incrementProgressBy(1);
            }
        });
    }
    void closeProgressDialog() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                dialog.dismiss();
            }
        });
    }

    /**
     * Basic method to check if there is an internet connection
     * @return true if an internet connection is available, false otherwise
     */
    private boolean isInternetConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }


    /**
     * Success and error notifiers
     */
    void notifyRssUpdateError(final String str) {
        Log.e(TAG, str);
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getContext(), str, Toast.LENGTH_LONG).show();
            }
        });
    }

    void notifyRssUpdateSuccess(final String str) {
        if (Cobalt.DEBUG) Log.d(TAG, str);
        Snackbar snackbar = Snackbar.make(getView(), str, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(Color.BLACK);
        snackbar.show();
    }

    /**
     * Cobalt methods
     */

    @Override
    protected boolean onUnhandledEvent(String event, JSONObject data, String callback) {
        if (Cobalt.DEBUG) Log.d(TAG, "Received event " + event + " from web  with data: [" + (data == null ? "null" : data.toString()) + "]");
        switch (event) {
            case "addRss":
                if (data != null) {
                    String url = null;
                    try {
                        url = data.getString("url");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (URLUtil.isValidUrl(url)) {
                        DatabaseHelper.getInstance(getActivity()).addRssUrl(url);
                        notifyRssUpdateSuccess(getResources().getString(R.string.done_adding));
                        getRss();
                        return true;
                    } else {
                        notifyRssUpdateError(getResources().getString(R.string.err_url) + url);
                    }
                }
                break;
            case "updateRss":
                getRss();
                return true;
            case "removeRss":
                if (data != null) {
                    String url = null;
                    try {
                        url = data.getString("url");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    DatabaseHelper.getInstance(getActivity()).removeRss(url);
                    getRss();
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    protected boolean onUnhandledCallback(String callback, JSONObject data) {
        if (Cobalt.DEBUG) Log.d(TAG, "Received callback " + callback + " with data [" + data + "]");
        return false;
    }

    @Override
    protected void onUnhandledMessage(JSONObject message) {
        if (Cobalt.DEBUG) Log.d(TAG, "Received message [" + message + "]");
    }
}
