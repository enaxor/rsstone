package eu.rxsoft.xarone.rsstone;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.cobaltians.cobalt.Cobalt;

import java.util.ArrayList;
import java.util.List;

/**
 * rsstone
 * Created by @xarone on 9/24/16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";
    private static DatabaseHelper instance = null;
    private Context context;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "RSSTONE_DB";

    private static final String RSS_TABLE = "RSS";
    private static final String COL_RSS_URL = "URL";

    public void onCreate(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE " + RSS_TABLE + " (" + COL_RSS_URL + " TEXT) ");

        // test actions
        addRssUrl(database, "http://feeds.bbci.co.uk/news/world/rss.xml");
        addRssUrl(database, "http://rss.slashdot.org/Slashdot/slashdotMain");
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context.getApplicationContext());
        }
        return instance;
    }

    /**
     * Constructor is private to prevent direct instantiation
     * Call made to static method getInstance() instead
     */
    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void addRssUrl(SQLiteDatabase database, String uri) {
        ContentValues values = new ContentValues();
        values.put(COL_RSS_URL, uri);
        if (Cobalt.DEBUG) Log.d(TAG, "Add RSS flux for url " + uri);
        database.replace(RSS_TABLE, null, values);
    }

    public void addRssUrl(String uri) {
        SQLiteDatabase database = instance.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL_RSS_URL, uri);
        if (Cobalt.DEBUG) Log.d(TAG, "Add RSS flux for url " + uri);
        database.replace(RSS_TABLE, null, values);
    }

    public void removeRss(String uri) {
        SQLiteDatabase database = instance.getWritableDatabase();
        if (Cobalt.DEBUG) Log.d(TAG, "Remove RSS flux for url " + uri);
        database.delete(RSS_TABLE, COL_RSS_URL + " = ?", new String[]{uri});
    }

    public List<String> getRssUrl() {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT " + COL_RSS_URL + " FROM " + RSS_TABLE, null);
        List<String> ret = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                ret.add(cursor.getString(cursor.getColumnIndexOrThrow(COL_RSS_URL)));
            } while (cursor.moveToNext());
        }
        if (Cobalt.DEBUG) Log.d(TAG, "getRssUrl() return " + ret.toString());
        cursor.close();
        return ret;
    }
}
