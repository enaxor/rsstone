package eu.rxsoft.xarone.rsstone;

import org.cobaltians.cobalt.activities.CobaltActivity;
import org.cobaltians.cobalt.fragments.CobaltFragment;

/**
 * rsstone
 * Created by @xarone on 10/24/16.
 */
public class MainActivity extends CobaltActivity {

    @Override
    protected CobaltFragment getFragment() {
        return new MainFragment();
    }
}
