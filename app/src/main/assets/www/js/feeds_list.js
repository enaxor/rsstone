app.page = {
    name: 'feedslist',
    templates: ['feed_item'],
    init: function(data) {
        $('#feed-list').html(templates.apply('feed_item', {
            feed: data.items
        }));
        cobalt.nativeBars.setBarContent({
            "title": data.title
        });
    }
};
app.start();
