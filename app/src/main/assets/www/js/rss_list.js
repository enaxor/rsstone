app.page = {
    name: 'rsslist',
    templates: ['rss_item'],
    feeds: [],
    init: function(data) {
        cobalt.sendEvent("updateRss");
    },
    events: {
        "feeds": function(data, callback) {
            var ret = [];
            app.page.feeds = data;
            $.each(data, function(key, value) {
                ret.push({
                    id: key,
                    title: value['title'],
                    link: value['link'],
                    count: value['count']
                });
            });
            // apply template
            $('#rss-list').html(templates.apply('rss_item', {
                rss: ret
            }));
            app.touch('.rssitem', function() {
                var id = $(this).find('input[name="id"]').val();
                cobalt.navigate.push({
                    page: "feeds_list.html",
                    controller: "feedslist",
                    data: {
                        items: app.page.feeds[id].feeds,
                        title: app.page.feeds[id].title
                    },
                    animated: false
                });
            });
            $('.rssitem').longTap(function() {
                var id = $(this).find('input[name="id"]').val();
                var title = app.page.feeds[id].title;
                var link = app.page.feeds[id].feedUrl;
                cobalt.alert({
                    title: "Delete rss flux",
                    message: "This will remove '" + title + "' from the list.",
                    buttons: ["Remove", "Cancel"],
                    callback: function(data) {
                        if (data.index == 0) {
                            cobalt.sendEvent("removeRss", {
                                url: link
                            });
                        }
                    }
                });
            });
            app.touch('.rss-main-url', function() {
                cobalt.openExternalUrl($(this).data('url'));
            });
        },
        "onWebLayerDismissed": function(data, callback) {
            // closed dialogs
            if (data.page == "dialog_add_rss.html" && data.data.url.trim()) {
                cobalt.sendEvent("addRss", data.data);
            }
        }
    },
    onBarButtonPressed: function(buttonName) {
        switch (buttonName) {
            case "refresh":
                cobalt.toast("Refreshing Feeds...");
                cobalt.sendEvent("updateRss");
                break;
            case "add":
                cobalt.webLayer("show", "dialog_add_rss.html");
                break;
            case "about":
                cobalt.webLayer("show", "dialog_about.html");
                break;
        }
    }
};
app.start();