app.page = {
    name: 'webnavigation',
    init: function(data) {
        window.location.href = data.url;
    }
};
app.start();
