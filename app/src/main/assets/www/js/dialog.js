app.page = {
    name: 'dialog',
    init: function(data) {
        app.touch('.dialog-close-button', function() {
            cobalt.webLayer("dismiss");
        });
        app.touch('.dialog-add-button', function() {
            cobalt.webLayer("dismiss", { "url" : $('#rssField').val() });
        })
    },
    events: {
        "onBackButtonPressed": function(data, callback) {
            cobalt.webLayer("dismiss");
        }
    }
};
app.start();