var app = {
    debug: true,
    //debugInBrowser: true,
    //debugTemplates : true,
    log: function(a, b, c, d, e, f, g, h, i) {
        if (app.debug) {
            cobalt.log(a, b, c, d, e, f, g, h, i);
        }
    },
    touchTimer: null,
    touch: function(selector, touchHandler, allowDefault) {
        var preventDefault = allowDefault ? false : true;
        var elem = $(selector)
        var touchup = function() {
            elem.removeClass('touched');
        };
        var touching = function(e) {
            if (!elem.hasClass('touched')) {
                elem.addClass('touched');
                clearTimeout(app.touchTimer);
                app.touchTimer = setTimeout(touchup, 1000);
                touchHandler.apply([this, e]);
            }
            if (preventDefault)
                return false;
        };
        elem.unbind('tap').on('tap', touching);
        elem.unbind('click').on('click', touching);
    },
    navigate: {
        _do: function(method, options) {
            if (app.debugInBrowser) {
                cobalt.storage.set('kristal_debug_nav_data', (options && options.data) || undefined);
            }
            cobalt.navigate[method](options);
        },
        push: function(options) {
            this._do('push', options);
        },
        modal: function(options) {
            this._do('modal', options);
        },
        pop: function(options) {
            cobalt.navigate.pop();
        },
        dismiss: function(options) {
            this._do('dismiss', options);
        },
        popTo: function(options) {
            this._do('popTo', options);
        },
        replace: function(options) {
            this._do('replace', options);
        }
    },
    start: function() {
        app.page = $.extend(true, {}, app.super, app.page);
        $('body').addClass(app.page.name);

        if (window.templates) {
            $.each(app.page.templates, function(key, item) {
                templates.templates.push(item);
            });
            templates.init();
            templates.addAsPartials(app.page.partials);
        }

        cobalt.init({
            events: app.page.events,
            debug: true,
            debugInBrowser: app.debugInBrowser,
            debugInDiv: app.debugInDiv
        });
        $('body').addClass(cobalt.platform.is);

        if (app.debugInBrowser) {
            setTimeout(app.page.events.onAppStarted, 200);
            setTimeout(app.page.events.onPageShown, 250);
        }
    },
    /*
     'super' is the default/parent controller for all pages.
     When defining a page you can use only a subset of functions and values.
     This "heritage" is recursive so you can mix some templates here and in the children page
     In a page you can also redefine a function and call app.super.myFunction if needed.
     */
    super: {
        name: 'default',
        templates: [],
        partials: [],
        config: function(data) {
            cobalt.nativeBars.setEventListener(app.page.onBarButtonPressed);
        },
        init: function(data) {
            app.page.reset(data);
        },
        onBarButtonPressed: function(buttonName) {
            app.log('onBarButtonPressed ', buttonName);
        },
        events: {
            "onBackButtonPressed": function() {
                app.navigate.pop();
            },
            "onPageShown": function(data) {
                if (!app.pageAlreadyShown) {
                    app.page.config(data);
                    app.page.init(data);
                    app.pageAlreadyShown = true;
                }
            }
        }
    }
};
