# rsstone
Rsstone is a simple but fonctional RSS viewer for Android using [Cobalt](https://github.com/cobaltians/Cobalt).

## Issues and features
[The issues page](https://github.com/xarone/rsstone/issues) is for reporting bugs and feature requests.

## License
* rsstone is licensed under the GPL and will always be free.
* [License of the used library](https://github.com/ahorn/android-rss)

![screenshot.png](art/screenshot.png)
